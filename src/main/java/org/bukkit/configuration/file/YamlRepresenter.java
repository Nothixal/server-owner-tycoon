package org.bukkit.configuration.file;

import org.bukkit.configuration.ConfigurationSection;
import org.jetbrains.annotations.NotNull;
import org.yaml.snakeyaml.nodes.Node;
import org.yaml.snakeyaml.representer.Representer;

public class YamlRepresenter extends Representer {

  public YamlRepresenter() {
    this.multiRepresenters.put(ConfigurationSection.class, new RepresentConfigurationSection());
  }

  private class RepresentConfigurationSection extends RepresentMap {

    @NotNull
    @Override
    public Node representData(@NotNull Object data) {
      return super.representData(((ConfigurationSection) data).getValues(false));
    }
  }

}
