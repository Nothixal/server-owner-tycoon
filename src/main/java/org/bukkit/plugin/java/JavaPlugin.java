package org.bukkit.plugin.java;

import com.google.common.base.Charsets;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.net.URL;
import java.net.URLConnection;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.PluginBase;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Represents a Java plugin
 */
public abstract class JavaPlugin extends PluginBase {

  private boolean isEnabled = false;
  private File file = null;
  private File dataFolder = null;
  private ClassLoader classLoader = null;
  private boolean naggable = true;
  private FileConfiguration newConfig = null;
  private File configFile = null;
  private Logger logger = Logger.getLogger("System64");

  public JavaPlugin() {
//    final ClassLoader classLoader = this.getClass().getClassLoader();
//    if (!(classLoader instanceof PluginClassLoader)) {
//      throw new IllegalStateException("JavaPlugin requires " + PluginClassLoader.class.getName());
//    }
//    ((PluginClassLoader) classLoader).initialize(this);
  }

  /**
   * Returns the folder that the plugin data's files are located in. The folder may not yet exist.
   *
   * @return The folder.
   */
  @NotNull
  @Override
  public final File getDataFolder() {
    return dataFolder;
  }

  /**
   * Returns a value indicating whether or not this plugin is currently enabled
   *
   * @return true if this plugin is enabled, otherwise false
   */
  @Override
  public final boolean isEnabled() {
    return isEnabled;
  }

  /**
   * Returns the file which contains this plugin
   *
   * @return File containing this plugin
   */
  @NotNull
  protected File getFile() {
    return file;
  }

  @NotNull
  @Override
  public FileConfiguration getConfig() {
    if (newConfig == null) {
      reloadConfig();
    }
    return newConfig;
  }

  /**
   * Provides a reader for a text file located inside the jar.
   * <p>
   * The returned reader will read text with the UTF-8 charset.
   *
   * @param file the filename of the resource to load
   * @return null if {@link #getResource(String)} returns null
   * @throws IllegalArgumentException if file is null
   * @see ClassLoader#getResourceAsStream(String)
   */
  @Nullable
  protected final Reader getTextResource(@NotNull String file) {
    final InputStream in = getResource(file);

    return in == null ? null : new InputStreamReader(in, Charsets.UTF_8);
  }

  @Override
  public void reloadConfig() {
    newConfig = YamlConfiguration.loadConfiguration(configFile);

    final InputStream defConfigStream = getResource("config.yml");
    if (defConfigStream == null) {
      return;
    }

    newConfig.setDefaults(YamlConfiguration.loadConfiguration(new InputStreamReader(defConfigStream, Charsets.UTF_8)));
  }

  @Override
  public void saveConfig() {
    try {
      getConfig().save(configFile);
    } catch (IOException ex) {
      logger.log(Level.SEVERE, "Could not save config to " + configFile, ex);
    }
  }

  @Override
  public void saveDefaultConfig() {
    if (!configFile.exists()) {
      saveResource("config.yml", false);
    }
  }

  @Override
  public void saveResource(@NotNull String resourcePath, boolean replace) {
    if (resourcePath == null || resourcePath.equals("")) {
      throw new IllegalArgumentException("ResourcePath cannot be null or empty");
    }

    resourcePath = resourcePath.replace('\\', '/');
    InputStream in = getResource(resourcePath);
    if (in == null) {
      throw new IllegalArgumentException("The embedded resource '" + resourcePath + "' cannot be found in " + file);
    }

    File outFile = new File(dataFolder, resourcePath);
    int lastIndex = resourcePath.lastIndexOf('/');
    File outDir = new File(dataFolder, resourcePath.substring(0, lastIndex >= 0 ? lastIndex : 0));

    if (!outDir.exists()) {
      outDir.mkdirs();
    }

    try {
      if (!outFile.exists() || replace) {
        OutputStream out = new FileOutputStream(outFile);
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
          out.write(buf, 0, len);
        }
        out.close();
        in.close();
      } else {
        logger.log(Level.WARNING,
            "Could not save " + outFile.getName() + " to " + outFile + " because " + outFile.getName()
                + " already exists.");
      }
    } catch (IOException ex) {
      logger.log(Level.SEVERE, "Could not save " + outFile.getName() + " to " + outFile, ex);
    }
  }

  @Nullable
  @Override
  public InputStream getResource(@NotNull String filename) {
    if (filename == null) {
      throw new IllegalArgumentException("Filename cannot be null");
    }

    try {
      URL url = getClassLoader().getResource(filename);

      if (url == null) {
        return null;
      }

      URLConnection connection = url.openConnection();
      connection.setUseCaches(false);
      return connection.getInputStream();
    } catch (IOException ex) {
      return null;
    }
  }

  /**
   * Returns the ClassLoader which holds this plugin
   *
   * @return ClassLoader holding this plugin
   */
  @NotNull
  protected final ClassLoader getClassLoader() {
    return classLoader;
  }

  /**
   * Sets the enabled state of this plugin
   *
   * @param enabled true if enabled, otherwise false
   */
  protected final void setEnabled(final boolean enabled) {
    if (isEnabled != enabled) {
      isEnabled = enabled;

      if (isEnabled) {
        onEnable();
      } else {
        onDisable();
      }
    }
  }

  @Override
  public void onLoad() {
  }

  @Override
  public void onDisable() {
  }

  @Override
  public void onEnable() {
  }

  @Override
  public final boolean isNaggable() {
    return naggable;
  }

  @Override
  public final void setNaggable(boolean canNag) {
    this.naggable = canNag;
  }

  @NotNull
  @Override
  public Logger getLogger() {
    return logger;
  }

}
