package org.bukkit.plugin;

import java.io.File;
import java.io.InputStream;
import java.util.logging.Logger;
import org.bukkit.configuration.file.FileConfiguration;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Represents a Plugin
 * <p>
 * The use of {@link PluginBase} is recommended for actual Implementation
 */
public interface Plugin {

  /**
   * Returns the folder that the plugin data's files are located in. The folder may not yet exist.
   *
   * @return The folder
   */
  @NotNull
  public File getDataFolder();

  /**
   * Gets a {@link FileConfiguration} for this plugin, read through "config.yml"
   * <p>
   * If there is a default config.yml embedded in this plugin, it will be provided as a default for this Configuration.
   *
   * @return Plugin configuration
   */
  @NotNull
  public FileConfiguration getConfig();

  /**
   * Gets an embedded resource in this plugin
   *
   * @param filename Filename of the resource
   * @return File if found, otherwise null
   */
  @Nullable
  public InputStream getResource(@NotNull String filename);

  /**
   * Saves the {@link FileConfiguration} retrievable by {@link #getConfig()}.
   */
  public void saveConfig();

  /**
   * Saves the raw contents of the default config.yml file to the location retrievable by {@link #getConfig()}.
   * <p>
   * This should fail silently if the config.yml already exists.
   */
  public void saveDefaultConfig();

  /**
   * Saves the raw contents of any resource embedded with a plugin's .jar file assuming it can be found using {@link
   * #getResource(String)}.
   * <p>
   * The resource is saved into the plugin's data folder using the same hierarchy as the .jar file (subdirectories are
   * preserved).
   *
   * @param resourcePath the embedded resource path to look for within the plugin's .jar file. (No preceding slash).
   * @param replace if true, the embedded resource will overwrite the contents of an existing file.
   * @throws IllegalArgumentException if the resource path is null, empty, or points to a nonexistent resource.
   */
  public void saveResource(@NotNull String resourcePath, boolean replace);

  /**
   * Discards any data in {@link #getConfig()} and reloads from disk.
   */
  public void reloadConfig();

  /**
   * Returns a value indicating whether or not this plugin is currently enabled
   *
   * @return true if this plugin is enabled, otherwise false
   */
  public boolean isEnabled();

  /**
   * Called when this plugin is disabled
   */
  public void onDisable();

  /**
   * Called after a plugin is loaded but before it has been enabled.
   * <p>
   * When multiple plugins are loaded, the onLoad() for all plugins is called before any onEnable() is called.
   */
  public void onLoad();

  /**
   * Called when this plugin is enabled
   */
  public void onEnable();

  /**
   * Simple boolean if we can still nag to the logs about things
   *
   * @return boolean whether we can nag
   */
  public boolean isNaggable();

  /**
   * Set naggable state
   *
   * @param canNag is this plugin still naggable?
   */
  public void setNaggable(boolean canNag);

  /**
   * Returns the plugin logger associated with this server's logger. The returned logger automatically tags all log
   * messages with the plugin's name.
   *
   * @return Logger associated with this plugin
   */
  @NotNull
  public Logger getLogger();
}
