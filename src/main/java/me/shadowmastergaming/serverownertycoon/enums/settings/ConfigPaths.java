package me.shadowmastergaming.serverownertycoon.enums.settings;

public enum ConfigPaths {

  GIVEAWAY_TEXT_CHANNEL("giveaway.channelID"),
  GIVEAWAY_ROLE("giveaway.roleID"),

  GIVEAWAY_RESTART("giveaway.restartHour"),

  PRIZE_POOL("prizePool"),
  ;

  private final String path;

  ConfigPaths(String path) {
    this.path = path;
  }

  public String getPath() {
    return path;
  }
}
