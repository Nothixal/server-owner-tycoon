package me.shadowmastergaming.serverownertycoon.enums;

public enum Countries {

  ALGERIA("\uD83C\uDDE9\uD83C\uDDFF", "dz", "Algeria"),
  AFGHANISTAN("\uD83C\uDDE6\uD83C\uDDEB", "af", "Afghanistan"),
  ARGENTINA("\uD83C\uDDE6\uD83C\uDDF7", "ar", "Argentina"),
  AUSTRALIA("\uD83C\uDDE6\uD83C\uDDFA", "au", "Australia"),
  AUSTRIA("\uD83C\uDDE6\uD83C\uDDF9", "at", "Austria"),
  BAHAMAS("\uD83C\uDDE7\uD83C\uDDF8", "bs", "Bahamas"),
  BELGIUM("\uD83C\uDDE7\uD83C\uDDEA", "be", "Belgium"),
  BOLIVIA("\uD83C\uDDE7\uD83C\uDDF4", "bo", "Bolivia"),
  BRAZIL("\uD83C\uDDE7\uD83C\uDDF7", "br", "Brazil"),
  BULGARIA("\uD83C\uDDE7\uD83C\uDDEC", "bg", "Bulgaria"),
  CANADA("\uD83C\uDDE8\uD83C\uDDE6", "ca", "Canada"),
  CHILE("\uD83C\uDDE8\uD83C\uDDF1", "cl", "Chile"),
  CHINA("\uD83C\uDDE8\uD83C\uDDF3", "cn", "China"),
  COLOMBIA("\uD83C\uDDE8\uD83C\uDDF4", "co", "Colombia"),
  COSTA_RICA("\uD83C\uDDE8\uD83C\uDDF7", "cr", "Costa Rica"),
  CROATIA("\uD83C\uDDED\uD83C\uDDF7", "hr", "Croatia"),
  CUBA("\uD83C\uDDE8\uD83C\uDDFA", "cu", "Cuba"),
  DENMARK("\uD83C\uDDE9\uD83C\uDDF0", "dk", "Denmark"),
  DOMINICAN_REPUBLIC("\uD83C\uDDE9\uD83C\uDDF4", "do", "Dominican Republic"),
  ECUADOR("\uD83C\uDDEA\uD83C\uDDE8", "ec", "Ecuador"),
  EGYPT("\uD83C\uDDEA\uD83C\uDDEC", "eg", "Egypt"),
  FINLAND("\uD83C\uDDEB\uD83C\uDDEE", "fi", "Finland"),
  FRANCE("\uD83C\uDDEB\uD83C\uDDF7", "fr", "France"),
  GERMANY("\uD83C\uDDE9\uD83C\uDDEA", "de", "Germany"),
  GREECE("\uD83C\uDDEC\uD83C\uDDF7", "gr", "Greece"),
  GREENLAND("\uD83C\uDDEC\uD83C\uDDF1", "gl", "Greenland"),
  GUATEMALA("\uD83C\uDDEC\uD83C\uDDF9", "gt", "Guatemala"),
  GUAM("\uD83C\uDDEC\uD83C\uDDFA", "gu", "Guam"),
  HAITI("\uD83C\uDDED\uD83C\uDDF9", "ht", "Haiti"),
  HONDURAS("\uD83C\uDDED\uD83C\uDDF3", "hn", "Honduras"),
  HUNGARY("\uD83C\uDDED\uD83C\uDDFA", "hu", "Hungary"),
  ICELAND("\uD83C\uDDEE\uD83C\uDDF8", "is", "Iceland"),
  INDIA("\uD83C\uDDEE\uD83C\uDDF3", "in", "India"),
  INDONESIA("\uD83C\uDDEE\uD83C\uDDE9", "id", "Indonesia"),
  IRAN("\uD83C\uDDEE\uD83C\uDDF7", "ir", "Iran"),
  IRAQ("\uD83C\uDDEE\uD83C\uDDF6", "iq", "Iraq"),
  IRELAND("\uD83C\uDDEE\uD83C\uDDEA", "ie", "Ireland"),
  ISRAEL("\uD83C\uDDEE\uD83C\uDDF1", "il", "Israel"),
  ITALY("\uD83C\uDDEE\uD83C\uDDF9", "it", "Italy"),
  JAMAICA("\uD83C\uDDEF\uD83C\uDDF2", "jm", "Jamaica"),
  JAPAN("\uD83C\uDDEF\uD83C\uDDF5", "jp", "Japan"),
  KAZAKHSTAN("\uD83C\uDDF0\uD83C\uDDFF", "kz", "Kazakhstan"),
  KUWAIT("\uD83C\uDDF0\uD83C\uDDFC", "kw", "Kuwait"),
  LEBANON("\uD83C\uDDF1\uD83C\uDDE7", "lb" ,"Lebanon"),
  LIBERIA("\uD83C\uDDF1\uD83C\uDDF7", "lr" ,"Liberia"),
  MALAYSIA("\uD83C\uDDF2\uD83C\uDDFE", "my", "Malaysia"),
  MEXICO("\uD83C\uDDF2\uD83C\uDDFD", "mx", "Mexico"),
  MOROCCO("\uD83C\uDDF2\uD83C\uDDE6", "ma" ,"Morocco"),
  NETHERLANDS("\uD83C\uDDF3\uD83C\uDDF1", "nl", "Netherlands"),
  NEW_ZEALAND("\uD83C\uDDF3\uD83C\uDDFF", "nz", "New Zealand"),
  NICARAGUA("\uD83C\uDDF3\uD83C\uDDEE", "ni", "Nicaragua"),
  NIGERIA("\uD83C\uDDF3\uD83C\uDDEC", "ng", "Nigeria"),
  NORWAY("\uD83C\uDDF3\uD83C\uDDF4", "no", "Norway"),
  PAKISTAN("\uD83C\uDDF5\uD83C\uDDF0", "pk", "Pakistan"),
  PANAMA("\uD83C\uDDF5\uD83C\uDDE6", "pa", "Panama"),
  PARAGUAY("\uD83C\uDDF5\uD83C\uDDFE", "py", "Paraguay"),
  PERU("\uD83C\uDDF5\uD83C\uDDEA", "pe", "Peru"),
  PHILIPPINES("\uD83C\uDDF5\uD83C\uDDED", "ph", "Philippines"),
  POLAND("\uD83C\uDDF5\uD83C\uDDF1", "pl", "Poland"),
  PORTUGAL("\uD83C\uDDF5\uD83C\uDDF9", "pt", "Portugal"),
  ROMANIA("\uD83C\uDDF7\uD83C\uDDF4", "ro", "Romania"),
  RUSSIA("\uD83C\uDDF7\uD83C\uDDFA", "ru", "Russia"),
  SAUDI_ARABIA("\uD83C\uDDF8\uD83C\uDDE6", "sa", "Saudi Arabia"),
  SINGAPORE("\uD83C\uDDF8\uD83C\uDDEC", "sg", "Singapore"),
  SLOVAKIA("\uD83C\uDDF8\uD83C\uDDF0", "sk", "Slovakia"),
  SLOVENIA("\uD83C\uDDF8\uD83C\uDDEE", "si", "Slovenia"),
  SOMALIA("\uD83C\uDDF8\uD83C\uDDF4", "so", "Somalia"),
  SOUTH_AFRICA("\uD83C\uDDFF\uD83C\uDDE6", "za", "South Africa"),
  SOUTH_KOREA("\uD83C\uDDF0\uD83C\uDDF7", "kr", "South Korea"),
  SPAIN("\uD83C\uDDEA\uD83C\uDDF8", "es", "Spain"),
  SWEDEN("\uD83C\uDDF8\uD83C\uDDEA", "se", "Sweden"),
  SWITZERLAND("\uD83C\uDDE8\uD83C\uDDED", "ch", "Switzerland"),
  SYRIA("\uD83C\uDDF8\uD83C\uDDFE", "sy", "Syria"),
  THAILAND("\uD83C\uDDF9\uD83C\uDDED", "th", "Thailand"),
  TURKEY("\uD83C\uDDF9\uD83C\uDDF7", "tr", "Turkey"),
  UNITED_KINGDOM("\uD83C\uDDEC\uD83C\uDDE7", "gb", "United Kingdom"),
  UNITED_STATES("\uD83C\uDDFA\uD83C\uDDF8", "us", "United States"),
  URUGUAY("\uD83C\uDDFA\uD83C\uDDFE", "uy", "Uruguay"),
  VENEZUELA("\uD83C\uDDFB\uD83C\uDDEA", "ve", "Venezuela"),
  VIETNAM("\uD83C\uDDFB\uD83C\uDDF3", "vn", "Vietnam"),
  ;

  private final String unicodeValue;
  private final String abbreviation;
  private final String displayName;

  Countries(String unicode, String abbreviation, String displayName) {
    this.unicodeValue = unicode;
    this.abbreviation = abbreviation;
    this.displayName = displayName;
  }

  public String getUnicodeValue() {
    return unicodeValue;
  }

  public String getAbbreviation() {
    return abbreviation;
  }

  public String getDisplayName() {
    return displayName;
  }
}
