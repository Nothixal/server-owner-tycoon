package me.shadowmastergaming.serverownertycoon.enums;

public enum Reactions {

  CHECK_MARK_GREEN_BOX("\u2705"),
  RED_X("\u274C"),
  KEYCAP_ZERO("\u0030\u20E3"),
  KEYCAP_ONE("\u0031\u20E3"),
  KEYCAP_TWO("\u0032\u20E3"),
  KEYCAP_THREE("\u0033\u20E3"),
  KEYCAP_FOUR("\u0034\u20E3"),
  KEYCAP_FIVE("\u0035\u20E3"),
  KEYCAP_SIX("\u0036\u20E3"),
  KEYCAP_SEVEN("\u0037\u20E3"),
  KEYCAP_EIGHT("\u0038\u20E3"),
  KEYCAP_NINE("\u0039\u20E3"),
  KEYCAP_TEN("\uD83D\uDD1F"),
  SPARKLES("\u2728"),

  BUST_IN_SILHOUETE("\uD83D\uDC64"),
  CLOCK("\uD83D\uDD70"),
  EMAIL("\uD83D\uDCE7"),
  BUTTERFLY("\uD83E\uDD8B"),
  PERFORMING_ARTS("\uD83C\uDFAD"),

  PLAY_EMOJI("\u25B6"),   // ▶
  PAUSE_EMOJI("\u23F8"),  // ⏸
  STOP_EMOJI("\u23F9"),   // ⏹

  ARROW_LEFT("\u2B05"),
  ARROW_RIGHT("\u27A1"),
  ARROW_UP("\u2B06"),
  ARROW_DOWN("\u2B07"),
  ;

  private final String unicodeValue;

  Reactions(String unicodeValue) {
    this.unicodeValue = unicodeValue;
  }

  public String getUnicodeValue() {
    return unicodeValue;
  }
}
