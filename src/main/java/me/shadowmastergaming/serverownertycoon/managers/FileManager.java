package me.shadowmastergaming.serverownertycoon.managers;

import com.google.common.base.Charsets;
import com.google.common.io.ByteStreams;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import me.shadowmastergaming.serverownertycoon.SOTBot;
import me.shadowmastergaming.serverownertycoon.utils.logger.LogUtils;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class FileManager {

  private final SOTBot bot;

  private final String homeDirectory;
  private final String dataDirectory;
  private final String langDirectory;
  private final String logsDirectory;

  private File configYMLFile;
  private FileConfiguration configYMLData;

  private File dataYMLFile;
  public FileConfiguration dataYMLData;

  private File langYMLFile;
  public FileConfiguration langYMLData;

  private Map<String, File> missingDirectories = new HashMap<>();
  private List<String> createdDirectories = new ArrayList<>();
  private List<String> uncreatedDirectories = new ArrayList<>();

  private Map<String, File> missingFiles = new HashMap<>();
  private List<String> createdFiles = new ArrayList<>();
  private List<String> uncreatedFiles = new ArrayList<>();

  private Map<File, FileConfiguration> loadedFiles = new HashMap<>();

  public FileManager(SOTBot bot) {
    this.bot = bot;

    this.homeDirectory = System.getProperty("user.dir");
    this.dataDirectory = homeDirectory + File.separator + "data";
    this.langDirectory = homeDirectory + File.separator + "lang";
    this.logsDirectory = homeDirectory + File.separator + "logs";
  }

  public void checkForAndCreateFiles() {
    if (!createDirectories()) {
      System.out.println("An error occurred whilst creating the directories.");
      System.out.println("Disabling Bot");
      System.exit(2);
      return;
    }

    if (!createFiles()) {
      System.out.println("An error occurred whilst creating the files.");
      System.out.println("Disabling Bot");
      System.exit(2);
      return;
    }

    loadFiles();
  }

  private void checkForMissingDirectories() {
    File homeDir = new File(homeDirectory);
    File data = new File(dataDirectory);
    File lang = new File(langDirectory);
    File logs = new File(logsDirectory);

    if (!directoryExists(homeDir)) {
      missingDirectories.put(homeDir.getName(), homeDir);
    }

    if (!directoryExists(lang)) {
      missingDirectories.put("Lang", lang);
    }

    if (!directoryExists(data)) {
      missingDirectories.put("Data", data);
    }

    if (!directoryExists(logs)) {
      missingDirectories.put("Logs", logs);
    }
  }

  private boolean createDirectories() {
    checkForMissingDirectories();

    LogUtils.logInfo("-={ Files }=-");

    if (missingDirectories.size() > 0) {
      List<String> missingDirectoriesKeys = new ArrayList<>(missingDirectories.keySet());

      String unaccountedDirectories = getListAsSortedString(missingDirectoriesKeys, "&e");

      LogUtils.logInfo("Missing Directories: " + unaccountedDirectories);
      LogUtils.logInfo("Attempting Creation...");

      for (Map.Entry<String, File> entry : missingDirectories.entrySet()) {
        if (entry.getValue().exists()) {
          continue;
        }

        if (createDirectory(entry.getValue())) {
          createdDirectories.add(entry.getKey());
        } else {
          uncreatedDirectories.add(entry.getKey());
        }
      }

      if (uncreatedDirectories.size() > 0) {
        boolean check = true;

        String uncreatedDirs = getListAsSortedString(uncreatedDirectories, "&c");

        LogUtils.logInfo("Missing Directories: " + uncreatedDirs);

        if (uncreatedDirs.contains(homeDirectory)) {
          check = false;
        }

        if (!check) {
          LogUtils.logError(uncreatedDirs + " directories failed to generate. Disabling plugin.");
          return false;
        }

        return true;
      }

      LogUtils.logInfo("&aSUCCESS: &7All missing directories were created successfully.");
      LogUtils.logInfo(" ");
      return true;
    }

    LogUtils.logInfo("Missing Directories: &aNone");
    return true;
  }

  private void checkForMissingFiles() {
    this.configYMLFile = new File(homeDirectory, "config.yml");
    this.dataYMLFile = new File(dataDirectory, "giveaway.yml");
    this.langYMLFile = new File(langDirectory, "lang_en.yml");

    if (!fileExists(configYMLFile)) {
      missingFiles.put("config.yml", configYMLFile);

      saveResource("config.yml", false);
    }

    if (!fileExists(dataYMLFile)) {
      missingFiles.put("giveaway.yml", dataYMLFile);

      saveResource("data/giveaway.yml", true);

      //this.plugin.reloadData();
    }

    if (!fileExists(langYMLFile)) {
      missingFiles.put("lang_en.yml", langYMLFile);

      saveResource("lang/lang_en.yml", true);
      reloadMessageConfig();
    }
  }

  private boolean createFiles() {
    checkForMissingFiles();

    if (missingFiles.size() > 0) {
      List<String> missingFilesKeys = new ArrayList<>(missingFiles.keySet());

      String unaccountedFiles = getListAsSortedString(missingFilesKeys, "&e");

      LogUtils.logInfo("Missing Files: " + unaccountedFiles);
      LogUtils.logInfo("Attempting Creation...");

      for (Map.Entry<String, File> entry : missingFiles.entrySet()) {
        if (entry.getValue().exists()) {
          continue;
        }

        if (createFile(entry.getValue())) {
          createdFiles.add(entry.getKey());
        } else {
          uncreatedFiles.add(entry.getKey());
        }
      }

      if (uncreatedFiles.size() > 0) {
        boolean check = true;

        String test = getListAsSortedString(uncreatedFiles, "&c");

        LogUtils.logInfo("Missing Files: " + test);

        if (test.contains("config.yml")) {
          check = false;
        }

        if (!check) {
          LogUtils.logError(test + " files failed to generate. Disabling plugin.");
          return false;
        }

        return true;
      }

      LogUtils.logInfo("&aSUCCESS: &7All missing files were created successfully.");
      LogUtils.logInfo(" ");
      return true;
    }

    LogUtils.logInfo("Missing Files: &aNone");
    return true;
  }

  private void loadFiles() {
    configYMLData = YamlConfiguration.loadConfiguration(configYMLFile);
    loadedFiles.put(configYMLFile, configYMLData);

    dataYMLData = YamlConfiguration.loadConfiguration(dataYMLFile);
    loadedFiles.put(dataYMLFile, dataYMLData);

    langYMLData = YamlConfiguration.loadConfiguration(langYMLFile);
    loadedFiles.put(langYMLFile, langYMLData);
  }

  private void reloadMessageConfig() {
    langYMLData = YamlConfiguration.loadConfiguration(langYMLFile);

    final InputStream defConfigStream = getResource("lang/lang_en.yml");
    if (defConfigStream == null) {
      return;
    }

    final YamlConfiguration defConfig;

    final byte[] contents;
    defConfig = new YamlConfiguration();
    try {
      contents = ByteStreams.toByteArray(defConfigStream);
    } catch (final IOException e) {
      LogUtils.logError("Unexpected failure reading lang_en.yml");
      return;
    }

    final String text = new String(contents, Charset.defaultCharset());
    if (!text.equals(new String(contents, Charsets.UTF_8))) {
      LogUtils.logWarning("Default system encoding may have misread lang_en.yml from plugin jar");
    }

    try {
      defConfig.loadFromString(text);
    } catch (final InvalidConfigurationException e) {
      LogUtils.logError("Cannot load configuration from jar");
    }

    langYMLData.setDefaults(defConfig);
  }

  private String getListAsSortedString(List<String> list, String colorCode) {
    StringBuilder stringBuilder = new StringBuilder();
    // Looping through the list.
    for (int i = 0; i < list.size(); i++) {
      //append the value into the builder
      stringBuilder.append(colorCode + list.get(i));

      // If the value is not the last element of the list then append a comma(,).
      if (i != list.size() - 1) {
        stringBuilder.append("&7, ");
      }
    }
    return stringBuilder.toString();
  }

  public boolean createDirectory(File file) {
    boolean isDirectoryCreated = directoryExists(file);

    if (!isDirectoryCreated) {
      isDirectoryCreated = file.mkdirs();
    }

    return isDirectoryCreated;
  }

  private boolean createFile(File file) {
    boolean fileCreated = file.exists();

    if (!fileCreated) {
      try {
        fileCreated = file.createNewFile();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    if (!fileCreated) {
      System.out.println("An error occurred while creating the player's data.");
      return false;
    }

    return true;
  }

  private boolean directoryExists(File file) {
    return file.exists();
  }

  private boolean fileExists(File file) {
    return directoryExists(file);
  }

  private File getFileFromResources(String fileName) {

    ClassLoader classLoader = getClass().getClassLoader();

    URL resource = classLoader.getResource(fileName);
    if (resource == null) {
      throw new IllegalArgumentException("file is not found!");
    } else {
      return new File(resource.getFile());
    }

  }

  public InputStream getResource(String filename) {
    try {
      URL url = bot.getClass().getClassLoader().getResource(filename);
      if (url == null) {
        return null;
      } else {
        URLConnection connection = url.openConnection();
        connection.setUseCaches(false);
        return connection.getInputStream();
      }
    } catch (IOException var4) {
      return null;
    }
  }

  public void saveResource(String resourcePath, boolean replace) {
    if (resourcePath != null && !resourcePath.equals("")) {
      resourcePath = resourcePath.replace('\\', '/');
      InputStream in = this.getResource(resourcePath);
      if (in == null) {
        throw new IllegalArgumentException("The embedded resource '" + resourcePath + "' cannot be found.");
      } else {
        File outFile = new File(homeDirectory, resourcePath);
        int lastIndex = resourcePath.lastIndexOf(47);
        File outDir = new File(homeDirectory, resourcePath.substring(0, Math.max(lastIndex, 0)));
        if (!outDir.exists()) {
          outDir.mkdirs();
        }

        try {
          if (outFile.exists() && !replace) {
            LogUtils.logWarning("Could not save " + outFile.getName() + " to " + outFile + " because " + outFile.getName() + " already exists.");
          } else {
            OutputStream out = new FileOutputStream(outFile);
            byte[] buf = new byte[1024];

            int len;
            while((len = in.read(buf)) > 0) {
              out.write(buf, 0, len);
            }

            out.close();
            in.close();
          }
        } catch (IOException var10) {
          LogUtils.logError("Could not save " + outFile.getName() + " to " + outFile);
        }

      }
    } else {
      throw new IllegalArgumentException("ResourcePath cannot be null or empty");
    }
  }

  //
  // Reload Methods
  //

  public void reloadDataYML() {
    dataYMLData = YamlConfiguration.loadConfiguration(dataYMLFile);
  }

  public void reloadLangYML() {
    langYMLData = YamlConfiguration.loadConfiguration(langYMLFile);
  }

  //
  // Getters & Setters
  //

  public File getConfigFile() {
    return configYMLFile;
  }

  public FileConfiguration getConfig() {
    return configYMLData;
  }

  public File getDataYMLFile() {
    return dataYMLFile;
  }

  public FileConfiguration getDataYMLData() {
    return dataYMLData;
  }

  public File getLangYMLFile() {
    return langYMLFile;
  }

  public FileConfiguration getLangYMLData() {
    return langYMLData;
  }

  //  public void createDefaultFiles() {
//    for (String fileName : Arrays.asList("List.yml", "Of.yml", "Your.yml", "Files.yml")) {
//      File file = new File(main.getDataFolder(), fileName); //Destination folder
//      InputStream inputStream = main.getResource("optional/path/location/in/jar/" + fileName); //If your file is in the src folder simply put fileName.
//      try {
//        FileUtils.copyInputStreamToFile(inputStream, file);
//      } catch (IOException e) {
//        e.printStackTrace();
//      }
//    }
//  }

}
