package me.shadowmastergaming.serverownertycoon.tasks;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import me.shadowmastergaming.serverownertycoon.SOTBot;
import me.shadowmastergaming.serverownertycoon.enums.settings.ConfigPaths;
import me.shadowmastergaming.serverownertycoon.utils.Utils;
import me.shadowmastergaming.serverownertycoon.utils.logger.LogUtils;
import me.shadowmastergaming.serverownertycoon.utils.text.TextUtils;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed.Field;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.TextChannel;

public class GiveawayTask {

  private SOTBot bot;

  // Scheduler Fields
  private ScheduledExecutorService executor;
  private ScheduledFuture<?> future;
  private ScheduledFuture<?> giveawayMessageUpdater;
  private ZonedDateTime now;
  private ZonedDateTime nextRun;

  // Giveaway Fields
  private int day = 1;
  private double prizePool = 1.00;
  private int giveawayNumber;
  private Map<Long, Integer> userGuesses = new HashMap<>();
  private List<Long> winners = new ArrayList<>();

  // Channel Fields
  private TextChannel textChannel;
  private EmbedBuilder embedBuilder;
  private Message giveawayMessage;
  private Role giveawayRole;

  private boolean giveawayInProgress = false;

  public GiveawayTask(SOTBot bot) {
    this.bot = bot;

    initialSetup();
  }

  public void initialSetup() {
    // Get the current time.
    now = ZonedDateTime.now(Clock.systemDefaultZone());

    // Get the time for the next giveaway cycle.
    nextRun = calculateNextTime(bot.getConfig().getInt(ConfigPaths.GIVEAWAY_RESTART.getPath(), 0));

    // Set the day of the year.
    day = Calendar.getInstance().get(Calendar.DAY_OF_YEAR);

    // Set the prize pool from the giveaway.yml
    // If the config can't find the value, the prize pool will default to $1.00.
    prizePool = bot.getGiveawayConfig().getDouble(ConfigPaths.PRIZE_POOL.getPath(), 1.00);

    // Generate a random number for the giveaway.
    giveawayNumber = generateRandomGiveawayNumber();

    // Setup the Embed Message that will be sent to the giveaway channel.
    embedBuilder = Utils.getGiveawayEmbed(day, prizePool);

    // Check to see if the config has saved user guesses.
    // If so, fill the userGuesses map with the values.
    if (bot.getGiveawayConfig().getConfigurationSection("userGuesses") != null) {
      // The Bukkit API does not allow for simple map conversions. All lists, sets, maps, etc will be returned in a Map<String, Object>.
      // This is the workaround for now.
      Map<String, Object> test = bot.getGiveawayConfig().getConfigurationSection("userGuesses").getValues(false);

      for (Map.Entry<String, Object> entry : test.entrySet()) {
        userGuesses.put(Long.parseLong(entry.getKey()), (int) entry.getValue());
      }

      // TODO: Comment this out before sending it.
      // Verify that the map prints out the correct values.
      for (Map.Entry<Long, Integer> entry : userGuesses.entrySet()) {
        System.out.println(entry.getKey() + ":" + entry.getValue());
      }
    }

    // Check to see if the config has a saved text channel. If so, assign it to the value.
    if (bot.getGiveawayConfig().get(ConfigPaths.GIVEAWAY_TEXT_CHANNEL.getPath()) != null) {
      textChannel = bot.getJda().getTextChannelById(bot.getGiveawayConfig().getLong(ConfigPaths.GIVEAWAY_TEXT_CHANNEL.getPath()));
    }

    // Check to see if the config has a saved giveaway role. If so, assign it to the value.
    if (bot.getGiveawayConfig().get(ConfigPaths.GIVEAWAY_ROLE.getPath()) != null) {
      giveawayRole = bot.getJda().getRoleById(bot.getGiveawayConfig().getLong(ConfigPaths.GIVEAWAY_ROLE.getPath()));
    }
  }

  private void saveData() {
    // Save the current prize pool amount.
    bot.getGiveawayConfig().set(ConfigPaths.PRIZE_POOL.getPath(), prizePool);

    // Save any user guesses.
    if (bot.getGiveawayConfig().getConfigurationSection("userGuesses") == null) {
      bot.getGiveawayConfig().createSection("userGuesses", userGuesses);
    } else {
      bot.getGiveawayConfig().set("userGuesses", userGuesses);
    }

    // Save all changes to the file.
    try {
      bot.getGiveawayConfig().save(bot.getGiveawayFile());
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void stopTaskEarly() {
    drawEarly();
    stopTask();
  }

  public void stopTask() {
    giveawayInProgress = false;
    future.cancel(true);
    executor.shutdownNow();

    saveData();
    LogUtils.logInfo("Giveaways have been stopped.");
  }

  public void startTask() {
    LogUtils.logInfo("The giveaway task has been started.");

    // Declare that a giveaway is now in progress.
    giveawayInProgress = true;

    // If the giveaway has been stopped previously, create a new executor.
    executor = Executors.newScheduledThreadPool(2);

    // Setup the Embed Message that will be sent to the giveaway channel.
    embedBuilder = Utils.getGiveawayEmbed(day, prizePool);

    //
    giveawayMessage = textChannel.sendMessage(giveawayRole.getAsMention()).embed(embedBuilder.build()).complete();

    // Generate a random number for the giveaway.
    giveawayNumber = generateRandomGiveawayNumber();

    // Find the duration in between now and the next run.
    Duration duration = Duration.between(now, nextRun);
    long initialDelay = duration.getSeconds();

    future = executor.scheduleAtFixedRate(
        new Runnable() {
          @Override
          public void run() {
            draw(textChannel);
          }
        }, initialDelay, TimeUnit.DAYS.toSeconds(1), TimeUnit.SECONDS);


    // Scheduled task to update the embed message for the time remaining.
    giveawayMessageUpdater = executor.scheduleAtFixedRate(
        new Runnable() {

          @Override
          public void run() {
            if (giveawayMessage == null) {
              return;
            }

            Message message = textChannel.retrieveMessageById(giveawayMessage.getIdLong()).complete();

            if (message == null) {
              return;
            }

            Date current = Date.from(Instant.now());
            Date next = Date.from(nextRun.toInstant());

            long timeRemaining = next.getTime() - current.getTime();

            embedBuilder = Utils.getGiveawayEmbed(day, prizePool);
            embedBuilder.getFields().set(1, new Field(":stopwatch: Time Left", TextUtils.bold(Utils.getTimeRemaining(timeRemaining)), true));
            giveawayMessage.editMessage(embedBuilder.build()).queue();
          }
        }, 1, 5, TimeUnit.SECONDS);

    //System.out.println(future.getDelay(TimeUnit.SECONDS));
  }

  private EmbedBuilder getWinnersEmbedMessage() {
// The logic for the winners.
    StringBuilder builder = new StringBuilder();
    EmbedBuilder embedBuilder = new EmbedBuilder();
    embedBuilder.setTitle("The Dollar Lottery - Day " + day);

    if (winners.size() == 1) {
      builder.append(bot.getJda().getUserById(winners.get(0)).getAsMention());

      embedBuilder.setDescription("Winner: " + builder.toString() + "\n\n" + "Prize Pool: " + TextUtils.bold("$" + Utils.moneyFormat.format(prizePool)));
    } else {
      for (int i = 0; i < winners.size(); i++) {
        builder.append(bot.getJda().getUserById(winners.get(i)).getAsMention());

        if (i != winners.size() - 1) {
          builder.append("\n");
        }
      }

      embedBuilder.setDescription("Winners: \n" + builder.toString() + "\n\n" + "Prize Pool: $" + prizePool);
    }

    return embedBuilder;
  }

  private void drawEarly() {
    if (userGuesses.isEmpty()) {
      prizePool++;
      day++;
      return;
    }

    for (Map.Entry<Long, Integer> entry : userGuesses.entrySet()) {
      if (entry.getValue() == giveawayNumber) {
        winners.add(entry.getKey());
      }
    }

    if (winners.isEmpty()) {
      prizePool++;
      day++;
      return;
    }

    textChannel.sendMessage(getWinnersEmbedMessage().build()).queue();

    saveData();

    clearWinners();
    clearUserGuesses();
  }

  private void draw(TextChannel textChannel) {
    // No one took a guess.
    // Prize pool should be increased by 1 and the next rotation will start.
    if (userGuesses.isEmpty()) {
      // Tell the channel that no one guessed and start the next day.
      prizePool++;
      day++;

      //textChannel.sendMessage("No one tried to guess a number. Prize pool is now " + prizePool).queue();
      //System.out.println("No one tried to guess a number. Prize pool is now " + prizePool);

      reset();
      return;
    }

    for (Map.Entry<Long, Integer> entry : userGuesses.entrySet()) {
      if (entry.getValue() == giveawayNumber) {
        winners.add(entry.getKey());
      }
    }

    if (winners.isEmpty()) {
      prizePool++;
      day++;

      //textChannel.sendMessage("No Winners. Prize pool is now " + prizePool).queue();
      //System.out.println("No Winners. Prize pool is now " + prizePool);

      reset();
      return;
    }

    textChannel.sendMessage(getWinnersEmbedMessage().build()).queue();

    //textChannel.sendMessage("Congratulations to: \n" + builder.toString() + "\n" + "You won " + prizePool).queue();

    prizePool = 1;
    day++;
    reset();
  }

  private void reset() {
    saveData();

    clearWinners();
    clearUserGuesses();
    giveawayNumber = generateRandomGiveawayNumber();

    embedBuilder = Utils.getGiveawayEmbed(day, prizePool);
    giveawayMessage.delete().complete();
    giveawayMessage = textChannel.sendMessage(giveawayRole.getAsMention()).embed(embedBuilder.build()).complete();

    now = ZonedDateTime.now(Clock.systemDefaultZone());
    nextRun = calculateNextTime(bot.getConfig().getInt(ConfigPaths.GIVEAWAY_RESTART.getPath(), 0));

    SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a");

    LogUtils.logInfo(" ");
    LogUtils.logInfo("-={ Giveaway }=-");
    LogUtils.logInfo("Today's Winning Number: " + giveawayNumber);
    LogUtils.logInfo("Today's Prize Pool: $" + prizePool);
    LogUtils.logInfo("Next Scheduled Giveaway Drawing: " + format.format(Date.from(nextRun.toInstant())));
    LogUtils.logInfo(" ");
  }

  private void clearWinners() {
    winners.clear();
  }

  private void clearUserGuesses() {
    userGuesses.clear();
  }

  private static int generateRandomGiveawayNumber() {
    return new Random().nextInt(100) + 1;
  }

  // TODO: Fix the method back to what it was.
  private ZonedDateTime calculateNextTime(int hour) {
    //ZonedDateTime nextRun = now.plusMinutes(1);

    ZonedDateTime nextRun = now.withHour(hour).withMinute(0).withSecond(0);

    if (now.compareTo(nextRun) > 0) {
      nextRun = nextRun.plusDays(1);
    }

    return nextRun;
  }

  public ScheduledFuture<?> getFuture() {
    return future;
  }

  public boolean isGiveawayInProgress() {
    return giveawayInProgress;
  }

  public void setGiveawayInProgress(boolean giveawayInProgress) {
    this.giveawayInProgress = giveawayInProgress;
  }

  public TextChannel getTextChannel() {
    return textChannel;
  }

  public void setTextChannel(TextChannel textChannel) {
    this.textChannel = textChannel;
  }

  public Map<Long, Integer> getUserGuesses() {
    return userGuesses;
  }

  public int getDay() {
    return day;
  }

  public void setDay(int day) {
    this.day = day;
  }

  public double getPrizePool() {
    return prizePool;
  }

  public void setPrizePool(double prizePool) {
    this.prizePool = prizePool;
  }

  public Role getGiveawayRole() {
    return giveawayRole;
  }

  public void setGiveawayRole(Role giveawayRole) {
    this.giveawayRole = giveawayRole;
  }
}
