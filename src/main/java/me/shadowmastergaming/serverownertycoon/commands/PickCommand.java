package me.shadowmastergaming.serverownertycoon.commands;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageHistory;
import net.dv8tion.jda.api.entities.User;

public class PickCommand extends Command {

  public PickCommand() {
    this.name = "pick";
  }

  @Override
  protected void execute(CommandEvent event) {
    event.getMessage().delete().queue();

    if (!event.getMember().hasPermission(Permission.ADMINISTRATOR)) {
      event.getTextChannel().sendMessage("This command can only be used by administrators!")
          .queue(message -> message.delete().queueAfter(7, TimeUnit.SECONDS));
      return;
    }

    try {
      MessageHistory history = new MessageHistory(event.getTextChannel());
      List<Message> msgs;

      msgs = history.retrievePast(1).complete();

      if (msgs.isEmpty()) {
        event.getTextChannel().sendMessage("There are no messages to retrieve!")
            .queue(message -> message.delete().queueAfter(5, TimeUnit.SECONDS));
        return;
      }

      Message messageAbove = msgs.get(0);
      System.out.println(messageAbove.toString());

      List<User> users = new ArrayList<>();

      if (messageAbove.getReactions().isEmpty()) {
        event.getTextChannel().sendMessage("The message above has no reactions!")
            .queue(message -> message.delete().queueAfter(5, TimeUnit.SECONDS));
        return;
      }

      messageAbove.getReactions().get(0).retrieveUsers().forEach(users::add);

//      // Debug
//      for (User user : users) {
//        System.out.println(user.getName());
//      }

      User winner = users.get(new Random().nextInt(users.size()));
      event.reply("The winner is " + winner.getAsMention() + "!");
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
