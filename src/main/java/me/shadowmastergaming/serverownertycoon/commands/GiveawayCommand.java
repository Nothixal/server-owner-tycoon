package me.shadowmastergaming.serverownertycoon.commands;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import java.io.IOException;
import me.shadowmastergaming.serverownertycoon.SOTBot;
import me.shadowmastergaming.serverownertycoon.enums.settings.ConfigPaths;
import me.shadowmastergaming.serverownertycoon.utils.Utils;
import net.dv8tion.jda.api.entities.PrivateChannel;
import net.dv8tion.jda.api.entities.TextChannel;

public class GiveawayCommand extends Command {

  private final SOTBot bot;

  public GiveawayCommand(SOTBot bot) {
    this.bot = bot;
    this.name = "giveaway";
    this.ownerCommand = true;
  }

  @Override
  protected void execute(CommandEvent event) {
    String[] args = event.getArgs().split("\\s+");

    int length = args.length;

    if (event.getArgs().isEmpty()) {
      event.getMessage().delete().queue();

      PrivateChannel channel = event.getAuthor().openPrivateChannel().complete();

      channel.sendMessage("Giveaway Commands\n"
          + "/giveaway start\n"
          + "/giveaway stop\n"
          + "/giveaway stopearly").queue();
      return;
    }

    if (length == 1) {
      if (args[0].equalsIgnoreCase("start")) {
        event.getMessage().delete().queue();

        if (bot.getGiveawayTask().getGiveawayRole() == null) {
          event.reply("The giveaway role needs to be defined first!\nUse /setgiveawayrole to register it!");
          return;
        }

        if (bot.getGiveawayTask().isGiveawayInProgress()) {
          event.reply("The giveaway process already in progress.");
        } else {
          startGiveaway(event.getTextChannel());
        }

        return;
      }

      if (args[0].equalsIgnoreCase("stopearly")) {
        event.getMessage().delete().queue();
        bot.getGiveawayTask().stopTaskEarly();
        event.reply("The giveaway has been ended early!");
        return;
      }

      if (args[0].equalsIgnoreCase("stop")) {
        event.getMessage().delete().queue();
        stopGiveaway();
        event.reply("The giveaway has been stopped.");
        return;
      }
    }

    if (length == 2) {
      if (!args[0].equalsIgnoreCase("setday")) {
        return;
      }

      if (!Utils.isInt(args[1])) {
        return;
      }

      int day = Integer.parseInt(args[1]);

      bot.getGiveawayTask().setDay(day);
      return;
    }
  }

  private void startGiveaway(TextChannel textChannel) {
    bot.getGiveawayConfig().set(ConfigPaths.GIVEAWAY_TEXT_CHANNEL.getPath(), textChannel.getIdLong());

    try {
      bot.getGiveawayConfig().save(bot.getGiveawayFile());
      bot.getFileManager().reloadDataYML();
    } catch (IOException e) {
      e.printStackTrace();
    }

    bot.getGiveawayTask().setTextChannel(textChannel);
    bot.getGiveawayTask().startTask();
  }

  private void stopGiveaway() {
    bot.getGiveawayTask().stopTask();
  }
}
