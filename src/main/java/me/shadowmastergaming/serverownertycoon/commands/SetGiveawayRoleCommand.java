package me.shadowmastergaming.serverownertycoon.commands;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import java.io.IOException;
import me.shadowmastergaming.serverownertycoon.SOTBot;
import me.shadowmastergaming.serverownertycoon.enums.settings.ConfigPaths;
import me.shadowmastergaming.serverownertycoon.utils.Utils;
import net.dv8tion.jda.api.entities.Role;
import org.bukkit.configuration.file.YamlConfiguration;

public class SetGiveawayRoleCommand extends Command {

  private final SOTBot bot;

  public SetGiveawayRoleCommand(SOTBot bot) {
    this.bot = bot;
    this.name = "setgiveawayrole";
  }

  @Override
  protected void execute(CommandEvent event) {
    event.getMessage().delete().queue();

    if (event.getArgs().isEmpty()) {
      event.reply("/setgiveawayrole <ID>");
      return;
    }

    String[] args = event.getArgs().split("\\s+");

    if (args.length == 1) {
      if (!Utils.isLong(args[0])) {
        event.reply("Not a valid role ID.");
        return;
      }

      Role role = event.getGuild().getRoleById(args[0]);

      if (role == null) {
        event.reply("That role does not exist!");
      } else {
        bot.getGiveawayTask().setGiveawayRole(role);
        event.reply("Giveaway role has been updated.");
        bot.getGiveawayConfig().set(ConfigPaths.GIVEAWAY_ROLE.getPath(), role.getIdLong());

        try {
          bot.getGiveawayConfig().save(bot.getGiveawayFile());
          bot.getFileManager().reloadDataYML();
        } catch (IOException e) {
          e.printStackTrace();
        }

        System.out.println(bot.getGiveawayConfig().getLong(ConfigPaths.GIVEAWAY_ROLE.getPath()));
      }
    }
  }
}
