package me.shadowmastergaming.serverownertycoon.commands;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import java.util.Calendar;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import me.shadowmastergaming.serverownertycoon.SOTBot;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed.Field;

public class TestCommand extends Command {

  private final SOTBot bot;

  public TestCommand(SOTBot bot) {
    this.bot = bot;
    this.name = "test";
  }

  @Override
  protected void execute(CommandEvent event) {
    event.getMessage().delete().queue();

    System.out.println(Calendar.getInstance().get(Calendar.DAY_OF_YEAR));

    EmbedBuilder embedBuilder = new EmbedBuilder();
    embedBuilder.setTitle("The Dollar Lottery - Day " + bot.getGiveawayTask().getDay());
    embedBuilder.setDescription("Welcome to the giveaway channel!");
    embedBuilder.addField("How to Play", "To join, enter a number between 1-100."
        + "\n"
        + "If no one guesses the right number, the prize pool will increase."
        + "\n"
        + "This process will repeat till the end of the year!", false);

//    embedBuilder.addField("How to play", "Everyday a random number between 1 and 100 will be generated."
//        + "\n"
//        + "Enter a number between 1 and 100 and you will be entered."
//        + "\n"
//        + "If your number is picked, you win the prize pool.", true);

    embedBuilder.addField("Time Left", "23:51:31", true);
    embedBuilder.addField("Current Prize Pool", "$" + bot.getGiveawayTask().getPrizePool(), true);

    Message message = event.getTextChannel().sendMessage(embedBuilder.build()).complete();

    ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();

    executor.schedule(() -> {
      embedBuilder.getFields().set(1, new Field("Time Left", "18:33:12", true));
      message.editMessage(embedBuilder.build()).queue();
    }, 10, TimeUnit.SECONDS);
  }
}
