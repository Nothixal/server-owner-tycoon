package me.shadowmastergaming.serverownertycoon.commands;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import java.util.List;
import me.shadowmastergaming.serverownertycoon.utils.Utils;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Guild;

public class CountryCommand extends Command {

  public CountryCommand() {
    this.name = "country";
  }

  @Override
  protected void execute(CommandEvent event) {
    event.getMessage().delete().queue();

    Guild guild = event.getGuild();
    List<net.dv8tion.jda.api.entities.Category> categories = guild.getCategoriesByName("New Users", true);
    if (categories.isEmpty()) {
      event.reply("There is no category called `New Users` to put the new channel in!");
      return;
    }

    net.dv8tion.jda.api.entities.Category category = categories.get(0);

    category.createTextChannel(event.getMember().getEffectiveName())
        .addPermissionOverride(event.getMember(), Permission.MESSAGE_READ.getRawValue(), 0L)
        .addPermissionOverride(guild.getPublicRole(), 0L, Permission.MESSAGE_READ.getRawValue())
        .queue(channel -> Utils.sendCountryList(event, channel, 1));
  }
}
