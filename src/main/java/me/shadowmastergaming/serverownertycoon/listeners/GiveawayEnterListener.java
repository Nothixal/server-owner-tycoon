package me.shadowmastergaming.serverownertycoon.listeners;

import java.util.List;
import javax.annotation.Nonnull;
import me.shadowmastergaming.serverownertycoon.SOTBot;
import me.shadowmastergaming.serverownertycoon.tasks.GiveawayTask;
import me.shadowmastergaming.serverownertycoon.utils.Utils;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageHistory;
import net.dv8tion.jda.api.entities.PrivateChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.priv.PrivateMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class GiveawayEnterListener extends ListenerAdapter {

  private final SOTBot bot;

  public GiveawayEnterListener(SOTBot bot) {
    this.bot = bot;
  }

  @Override
  public void onPrivateMessageReceived(@Nonnull PrivateMessageReceivedEvent event) {
    handlePrivateMessage(event.getChannel(), event.getAuthor(), event.getMessage());
  }

  public void handlePrivateMessage(PrivateChannel channel, User user, Message message) {
    if (user.isBot()) {
      return;
    }

    if (!bot.getGiveawayTask().isGiveawayInProgress()) {
      return;
    }

    if (message.getContentRaw().equalsIgnoreCase("purge")) {
      MessageHistory history = new MessageHistory(channel);

      List<Message> messages = history.retrievePast(50).complete();

      for (Message msg : messages) {
        if (msg.getAuthor().getIdLong() != bot.getJda().getSelfUser().getIdLong()) {
          continue;
        }

        msg.delete().queue();
      }

      return;
    }

    if (!Utils.isInt(message.getContentRaw())) {
      channel.sendMessage("That's not a number!").queue();
      return;
    }

    int number = Integer.parseInt(message.getContentRaw());

    if (!Utils.isInBounds(number, 1, 100)) {
      channel.sendMessage("That number is out of bounds.").queue();
      return;
    }

    GiveawayTask task = bot.getGiveawayTask();

    if (task.getUserGuesses().containsKey(user.getIdLong())) {
      channel.sendMessage(user.getAsMention() + " You've already entered!").queue();
    } else {
      task.getUserGuesses().put(user.getIdLong(), number);
      channel.sendMessage("You have entered with the number: " + number).queue();
    }
  }
}
