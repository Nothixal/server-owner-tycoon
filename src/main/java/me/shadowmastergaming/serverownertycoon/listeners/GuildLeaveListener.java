package me.shadowmastergaming.serverownertycoon.listeners;

import java.util.List;
import net.dv8tion.jda.api.entities.Category;
import net.dv8tion.jda.api.entities.GuildChannel;
import net.dv8tion.jda.api.events.guild.member.GuildMemberRemoveEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class GuildLeaveListener extends ListenerAdapter {

  @Override
  public void onGuildMemberRemove(GuildMemberRemoveEvent event) {
    if (event.getUser().isBot()) {
      return;
    }

    List<Category> categories = event.getGuild().getCategoriesByName("New Users", true);
    if (categories.isEmpty()) {
      return;
    }

    for (GuildChannel channel : categories.get(0).getChannels()) {
      if (channel.getName().equalsIgnoreCase(event.getMember().getEffectiveName())) {
        channel.delete().queue();
      }
    }
  }

}
