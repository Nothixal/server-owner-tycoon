package me.shadowmastergaming.serverownertycoon.listeners;

import java.util.List;
import me.shadowmastergaming.serverownertycoon.utils.Utils;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Category;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class GuildMemberJoinListener extends ListenerAdapter {

  public GuildMemberJoinListener() {
  }

  @Override
  public void onGuildMemberJoin(GuildMemberJoinEvent event) {
    if (event.getUser().isBot()) {
      return;
    }

    Guild guild = event.getGuild();
    List<Category> categories = guild.getCategoriesByName("New Users", true);
    if (categories.isEmpty()) {
      return;
    }

    net.dv8tion.jda.api.entities.Category category = categories.get(0);

    category.createTextChannel(event.getMember().getEffectiveName())
        .addPermissionOverride(event.getMember(), Permission.MESSAGE_READ.getRawValue(), 0L)
        .addPermissionOverride(guild.getPublicRole(), 0L, Permission.MESSAGE_READ.getRawValue())
        .queue(channel -> Utils.sendCountryList(event, channel, 1));
  }

}
