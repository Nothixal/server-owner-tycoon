package me.shadowmastergaming.serverownertycoon.listeners;

import java.util.List;
import net.dv8tion.jda.api.entities.Category;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.events.ReadyEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class BotReadyListener extends ListenerAdapter {

  public void onReady(ReadyEvent event) {
    for (Guild guild : event.getJDA().getGuilds()) {
      List<Category> categories = guild.getCategoriesByName("New Users", true);

      if (categories.isEmpty()) {
        guild.createCategory("New Users").complete();
        return;
      }
    }

//    event.getGuild().getOwner().getUser().openPrivateChannel().queue(new Consumer<PrivateChannel>() {
//      @Override
//      public void accept(PrivateChannel privateChannel) {
//        privateChannel.sendMessage("There is not a category called `New Users`").queue();
//      }
//    });
  }

}
