package me.shadowmastergaming.serverownertycoon.utils;

import com.jagrosh.jdautilities.command.CommandEvent;
import com.jagrosh.jdautilities.commons.utils.FinderUtil;
import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import me.shadowmastergaming.serverownertycoon.SOTBot;
import me.shadowmastergaming.serverownertycoon.enums.Countries;
import me.shadowmastergaming.serverownertycoon.enums.Reactions;
import me.shadowmastergaming.serverownertycoon.utils.text.TextUtils;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageReaction.ReactionEmote;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.exceptions.HierarchyException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public final class Utils {

  public static DecimalFormat moneyFormat = new DecimalFormat("###,###.##");

  public static String readJSON(File file, String value) {
    try {
      JSONParser parser = new JSONParser();
      //Use JSONObject for simple JSON and JSONArray for array of JSON.
      try {
        Object object = parser.parse(new FileReader(new File(file.getAbsoluteFile().toString())));
        //convert Object to JSONObject
        JSONObject jsonObject = (JSONObject) object;

        //Reading the String
        String val;
        val = (String) jsonObject.get(value);

        //Printing all the values
        //System.out.println("Token: " + token);
        return val;

      } catch (ParseException e) {
        e.printStackTrace();
      }

    } catch (IOException e) {
      e.printStackTrace();
    }

    return null;
  }

  private static List<Countries> openPage(int pageNumber, int entriesPerPage, List<Countries> list) {
    return list.subList((pageNumber - 1) * entriesPerPage, Math.min((pageNumber - 1) * entriesPerPage + entriesPerPage, list.size()));
  }

  private static void consensus(Guild guild, Member member, TextChannel channel, int pageNumber, boolean isCommand) {
    List<Countries> queue = getCountries();
    List<Countries> queueSublist;

    int entriesPerPage = 5;
    int maxPages = (queue.size() / entriesPerPage) + 1;

    if (pageNumber < 1) {
      return;
    }

    if (pageNumber > maxPages) {
      return;
    }

    if (queue.size() > entriesPerPage) {
      queueSublist = openPage(pageNumber, entriesPerPage, queue);
    } else {
      queueSublist = queue;
    }

    StringBuilder queueOutput = new StringBuilder();

    for (int i = 0; i < queueSublist.size(); i++) {
      queueOutput.append(queueSublist.get(i).getUnicodeValue()).append(" ");
      queueOutput.append(queueSublist.get(i).getDisplayName());

      if (i != queueSublist.size() - 1) {
        queueOutput.append("\n");
      }
    }

    EmbedBuilder embedBuilder = new EmbedBuilder();
    embedBuilder.setDescription(TextUtils.bold("HELLO") + " there, " + member.getUser().getName() + "!"
        + "\n"
        + "What country are you from? Click one of the reactions below!"
        + "\n"
        + queueOutput.toString()
        + "\n\n"
        + "Use the arrow reactions for more options!");

    if (pageNumber == 1) {
      embedBuilder.addField("Next Page", ":arrow_right:", true);
    } else if (pageNumber == maxPages) {
      embedBuilder.addField("Previous Page", ":arrow_left:", true);
    } else {
      embedBuilder.addField("Previous Page", ":arrow_left:", true);
      embedBuilder.addField("Next Page", ":arrow_right:", true);
      embedBuilder.addBlankField(true);
    }

    // If it is a command. Set the timeout to 5 minutes.
    // Else set it to 1 day.
    TimeUnit timeUnit = isCommand ? TimeUnit.MINUTES : TimeUnit.DAYS;
    int timeout = isCommand ? 5 : 1;

    System.out.println(timeUnit);
    System.out.println(timeout);

    //Message message = channel.sendMessage(embedBuilder.build()).complete();

    channel.sendMessage(embedBuilder.build()).queue(message -> {
      if (pageNumber == 1) {
        addCountry(queueSublist, message);
        message.addReaction(Reactions.ARROW_RIGHT.getUnicodeValue()).queue();
      } else if (pageNumber == maxPages) {
        message.addReaction(Reactions.ARROW_LEFT.getUnicodeValue()).queue();
        addCountry(queueSublist, message);
      } else {
        message.addReaction(Reactions.ARROW_LEFT.getUnicodeValue()).queue();
        addCountry(queueSublist, message);
        message.addReaction(Reactions.ARROW_RIGHT.getUnicodeValue()).queue();
      }

      List<String> countryNames = new ArrayList<>();

      for (Countries countries : Countries.values()) {
        countryNames.add(countries.getUnicodeValue());
      }

      SOTBot.getInstance().getWaiter().waitForEvent(MessageReactionAddEvent.class, reaction -> {
        if (reaction.getMessageIdLong() != message.getIdLong()) {
          return false;
        }

        if (reaction.getMember() != member) {
          return false;
        }

        if (reaction.getReactionEmote().getName().equals(Reactions.ARROW_RIGHT.getUnicodeValue())) {
          if (pageNumber == maxPages) {
            message.delete().queue();
            consensus(guild, member, channel, pageNumber, isCommand);
            return false;
          }

          message.delete().queue();
          consensus(guild, member, channel, pageNumber + 1, isCommand);
          return false;
        }

        if (reaction.getReactionEmote().getName().equals(Reactions.ARROW_LEFT.getUnicodeValue())) {
          if (pageNumber == 1) {
            message.delete().queue();
            consensus(guild, member, channel, pageNumber, isCommand);
            return false;
          }

          message.delete().queue();
          consensus(guild, member, channel, pageNumber - 1, isCommand);
          return false;
        }

        if (countryNames.contains(reaction.getReactionEmote().getName())) {
          return true;
        }

        return false;
      }, reaction -> {
        ReactionEmote reactionEmote = reaction.getReactionEmote();
        User user = member.getUser();

        String nickname;

        if (user.getName().length() >= 32) {
          nickname = user.getName().substring(0, user.getName().length() - reactionEmote.getName().length()) + reactionEmote.getName();
        } else {
          nickname = user.getName() + " " + reactionEmote.getName();
        }

        if (member.isOwner()) {
          sendError(channel, "I'm not allowed to modify the server owner!\n\n"
              + "This channel will automatically delete itself in 10 seconds.");

          deleteChannel(channel, 10);
          return;
        }

        try {
          member.modifyNickname(nickname).queue();
        } catch (HierarchyException ex) {
          sendError(channel, "Can't modify a member with higher or equal highest role than yourself!\n"
              + "Have an admin adjust your roles!\n\n"
              + "This channel will automatically delete itself in 10 seconds.");

          deleteChannel(channel, 10);
          return;
        }

        channel.delete().queue();

        FinderUtil.findRoles("Supporter", guild).stream().findFirst().ifPresent(role -> guild.addRoleToMember(member, role).queue());
      }, timeout, timeUnit, () -> {
        deleteChannel(channel, 2);
//        if (isCommand) {
//          deleteChannel(channel, 2);
//          return;
//        }
        // Member took too long to verify their self.
        //guild.kick(member, "Failed to pass verification.").queue();
      });

    });
  }

  public static void sendCountryList(GuildMemberJoinEvent event, TextChannel channel, int pageNumber) {
    consensus(event.getGuild(), event.getMember(), channel, pageNumber, false);
  }

  public static void sendCountryList(CommandEvent event, TextChannel channel, int pageNumber) {
    consensus(event.getGuild(), event.getMember(), channel, pageNumber, true);
  }

  private static void sendError(TextChannel channel, String message) {
    EmbedBuilder embedBuilder = new EmbedBuilder();
    embedBuilder.setColor(Color.RED);
    embedBuilder.setTitle(":x: ERROR");
    embedBuilder.setDescription(message);

    channel.sendMessage(embedBuilder.build()).queue();
  }

  private static void deleteChannel(TextChannel channel, int seconds) {
    channel.delete().queueAfter(seconds, TimeUnit.SECONDS);
  }

  private static void addCountry(List<Countries> queueSublist, Message message) {
    for (Countries country : queueSublist) {
      message.addReaction(country.getUnicodeValue()).queue();
    }
  }

  private static List<Countries> getCountries() {
    Countries[] countries = Countries.values();

    List<Countries> modifiedCountries = new ArrayList<>();
    modifiedCountries.add(Countries.UNITED_STATES);
    modifiedCountries.add(Countries.UNITED_KINGDOM);
    modifiedCountries.add(Countries.CANADA);
    modifiedCountries.add(Countries.FRANCE);
    modifiedCountries.add(Countries.INDIA);
    modifiedCountries.add(Countries.CHINA);
    modifiedCountries.add(Countries.JAPAN);
    modifiedCountries.add(Countries.GERMANY);
    modifiedCountries.add(Countries.RUSSIA);
    modifiedCountries.add(Countries.SPAIN);
    modifiedCountries.add(Countries.EGYPT);
    modifiedCountries.add(Countries.AUSTRALIA);
    modifiedCountries.add(Countries.GREECE);
    modifiedCountries.add(Countries.ITALY);
    modifiedCountries.add(Countries.SINGAPORE);
    modifiedCountries.add(Countries.BRAZIL);
    modifiedCountries.add(Countries.IRELAND);

    for (Countries country : countries) {
      if (modifiedCountries.contains(country)) {
        continue;
      }

      modifiedCountries.add(country);
    }

    return modifiedCountries;
  }

  public static void autoDeleteMessage(Message message, int seconds) {
    message.delete().queueAfter(seconds, TimeUnit.SECONDS);
  }

  public static boolean isInt(String s) {
    try {
      Integer.parseInt(s);
      return true;
    } catch (NumberFormatException ex) {
      return false;
    }
  }

  public static boolean isLong(String s) {
    try {
      Long.parseLong(s);
      return true;
    } catch (NumberFormatException ex) {
      return false;
    }
  }

  public static boolean isInBounds(int number, int lower, int upper) {
    return number >= lower && number <= upper;
  }

  public static String getTimeRemaining(long milliseconds) {
    int seconds = (int) (milliseconds / 1000) % 60;
    int minutes = (int) ((milliseconds / (1000 * 60)) % 60);
    int hours = (int) ((milliseconds / (1000 * 60 * 60)) % 24);

    if (seconds < 0) {
      seconds = 0;
    }

    if (minutes < 0) {
      minutes = 0;
    }

    return String.format("%02d:%02d:%02d", hours, minutes, seconds);
  }

  public static EmbedBuilder getGiveawayEmbed(int day, double prizePool) {
    EmbedBuilder embedBuilder = new EmbedBuilder();
    embedBuilder.setTitle("The Dollar Lottery - Day " + day);
    embedBuilder.setDescription("Welcome to the giveaway channel!");
    embedBuilder.addField(":book: How to Play",
        "To join, please pivate message me (the bot), " + TextUtils.bold("@" + SOTBot.getInstance().getJda().getSelfUser().getAsTag())
            + " with a number between 1-100. You may only enter one response per day and "
        + "may not edit your message. All other entries and edited messages will be ignored."
        + "\n\n"
        + "If there are any people that guessed the correct number, it will be announced here in this channel. Multiple winners = prize pool split. "
        + "Otherwise, if no one guesses the correct number, the prize pool will increase by $1 each day. "
        + "This giveaway process will be from now until December 25, 2020.", false);

    embedBuilder.addField(":stopwatch: Time Left", TextUtils.bold("24:00:00"), true);
    embedBuilder.addField(":dollar: Current Prize Pool", TextUtils.bold("$" + moneyFormat.format(prizePool)), true);

    return embedBuilder;
  }

  /**
   * Export a resource embedded into a Jar file to the local file path.
   *
   * @param resourceName ie.: "/SmartLibrary.dll"
   * @return The path to the exported resource
   * @throws Exception
   */
  public static String exportResource(String resourceName) throws Exception {
    InputStream stream = null;
    OutputStream resStreamOut = null;
    String jarFolder;
    try {
      stream = SOTBot.class.getResourceAsStream(resourceName); //note that each / is a directory down in the "jar tree" been the jar the root of the tree
      if (stream == null) {
        throw new Exception("Cannot get resource \"" + resourceName + "\" from Jar file.");
      }

      int readBytes;
      byte[] buffer = new byte[4096];
      jarFolder = new File(SOTBot.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath())
          .getParentFile().getPath().replace('\\', '/');
      resStreamOut = new FileOutputStream(jarFolder + resourceName);
      while ((readBytes = stream.read(buffer)) > 0) {
        resStreamOut.write(buffer, 0, readBytes);
      }
    } catch (Exception ex) {
      throw ex;
    } finally {
      resStreamOut.close();
      stream.close();
    }

    return jarFolder + resourceName;
  }

  // get file from classpath, resources folder
  private File getFileFromResources(String fileName) {

    ClassLoader classLoader = getClass().getClassLoader();

    URL resource = classLoader.getResource(fileName);
    if (resource == null) {
      throw new IllegalArgumentException("file is not found!");
    } else {
      return new File(resource.getFile());
    }

  }

  public static void printFile(File file) throws IOException {

    if (file == null) return;

    try (FileReader reader = new FileReader(file);
        BufferedReader br = new BufferedReader(reader)) {

      String line;
      while ((line = br.readLine()) != null) {
        System.out.println(line);
      }
    }
  }
}
