package me.shadowmastergaming.serverownertycoon.utils;

import java.io.File;
import net.dv8tion.jda.api.Permission;

public final class BotConstants {

  public static final File auth = new File("auth.json");

  public static final String AUTHOR_NAME = "Captain Autismo";
  public static final String AUTHOR_DISCRIMINATOR = "#4875";
  public static final String AUTHOR = AUTHOR_NAME + AUTHOR_DISCRIMINATOR;
  public static final String AUTHOR_WEBSITE = "TBA";
  public static final String SUPPORT_GUILD = "";

  public static final String BOT_ID = Utils.readJSON(auth, "clientID");
  public static final String BOT_OWNER_ID = Utils.readJSON(auth, "ownerID");
  public static final String BOT_CO_OWNER_ID = Utils.readJSON(auth, "coOwnerID");

  public static final String BOT_NAME = "Server Owner Tycoon";
  public static final String BOT_PREFIX = "/";
  public static final String BOT_ALT_PREFIX = "<@" + BOT_ID + ">";

  public static final String BOT_GUILD_ID = "";
  public static final String VERSION_RAW = "1.3";
  public static final String VERSION = "v" + VERSION_RAW;

  public static final int DEFAULT_VOLUME = 100;

  public static final Permission[] RECOMMENDED_PERMS = new Permission[]{Permission.MESSAGE_READ,
      Permission.MESSAGE_WRITE, Permission.MESSAGE_HISTORY, Permission.MESSAGE_ADD_REACTION,
      Permission.MESSAGE_EMBED_LINKS, Permission.MESSAGE_ATTACH_FILES, Permission.MESSAGE_MANAGE,
      Permission.MESSAGE_EXT_EMOJI,
      Permission.MANAGE_CHANNEL, Permission.VOICE_CONNECT, Permission.VOICE_SPEAK, Permission.NICKNAME_CHANGE};

}
