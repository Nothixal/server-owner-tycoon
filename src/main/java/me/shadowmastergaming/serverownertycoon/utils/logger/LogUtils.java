package me.shadowmastergaming.serverownertycoon.utils.logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import me.shadowmastergaming.serverownertycoon.SOTBot;
import me.shadowmastergaming.serverownertycoon.utils.text.TextUtils;

public final class LogUtils {

  private static SOTBot plugin = SOTBot.getInstance();
  private static final String consolePrefix = "&7[&aServer Owner Tycoon&7] ";
  private static final String consoleWarningPrefix = "&7[&eServer Owner Tycoon&7] ";
  private static final String consoleErrorPrefix = "&7[&cServer Owner Tycoon&7] ";

  private LogUtils(SOTBot plugin) {
    LogUtils.plugin = plugin;
  }

  public static void logInfo(String msg) {
    System.out.println(getTime() + TextUtils.translateANSIColorCodes(getConsolePrefix() + msg));
  }

  public static void logInfoNoPrefix(String msg) {
    System.out.println(getTime() + TextUtils.translateANSIColorCodes(msg));
  }

  public static void logWarning(String msg) {
    System.out.println(getTime() + TextUtils.translateANSIColorCodes(getConsoleWarningPrefix() + msg));
  }

  public static void logWarningNoPrefix(String msg) {
    System.out.println(getTime() + TextUtils.translateANSIColorCodes(msg));
  }

  public static void logError(String msg) {
    System.out.println(getTime() + TextUtils.translateANSIColorCodes(getConsoleErrorPrefix() + msg));
  }

  public static void logErrorNoPrefix(String msg) {
    System.out.println(getTime() + TextUtils.translateANSIColorCodes(msg));
  }

  private static String getTime() {
    LocalDateTime today = LocalDateTime.now();
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("hh:mm:ss.SSS");
    return formatter.format(today) + " ";
  }

  public static void logToFile(String message) {
    final DateFormat fileDateFormat = new SimpleDateFormat("yyyy EEE, MMMMM dd");
    final Date d = new Date();
    final String date = fileDateFormat.format(d);
    final DateFormat dateFormat = new SimpleDateFormat("hh:mm a");

    String newMessage;
    newMessage = "[" + dateFormat.format(d) + "] " + message;

    try {
      final File file = new File(plugin.getDataFolder().getAbsolutePath() + File.separator + "logs", date + ".txt");
      final File parent = file.getParentFile();
      if (!parent.exists()) {
        parent.mkdirs();
      }
      final FileWriter fw = new FileWriter(file, true);
      final PrintWriter pw = new PrintWriter(fw);
      pw.println(newMessage);
      pw.flush();
      pw.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private static String getConsolePrefix() {
    return consolePrefix;
  }

  private static String getConsoleWarningPrefix() {
    return consoleWarningPrefix;
  }

  private static String getConsoleErrorPrefix() {
    return consoleErrorPrefix;
  }
}