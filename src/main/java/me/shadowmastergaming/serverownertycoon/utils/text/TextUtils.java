package me.shadowmastergaming.serverownertycoon.utils.text;

import me.shadowmastergaming.serverownertycoon.enums.ANSIColor;

public class TextUtils {

  /**
   * Converts the color codes into ANSI color codes.
   * @param msg The message with color codes to convert.
   * */
  public static String translateANSIColorCodes(String msg) {
    return ANSIColor.translateColorCodes('&', msg);
  }

  /**
   * Capatalizes the first letter in the word you provide.
   * @param msg The word you want to capatalize.
   * */
  public static String capitalizeFirstLetter(String msg) {
    return msg.substring(0,1).toUpperCase() + msg.substring(1).toLowerCase();
  }

  /**
   * Formats text as bold.
   * @param msg The message you want to make bold.
   * */
  public static String bold(String msg) {
    return  "**" + msg + "**";
  }

  /**
   * Formats text as italic.
   * @param msg The message you want to make italic.
   * */
  public static String italic(String msg) {
    return  "*" + msg + "*";
  }

  /**
   * Formats text as underline.
   * @param msg The message you want to make underlined.
   * */
  public static String underline(String msg) {
    return "__" + msg + "__";
  }

  /**
   * Formats text as strikethrough.
   * @param msg The message you want to strikethrough.
   * */
  public static String strikethrough(String msg) {
    return "~~" + msg + "~~";
  }

  /**
   * Formats text as a spoiler.
   * @param msg The message you want to convert to a spoiler.
   * */
  public static String spoiler(String msg) {
    return  "||" + msg + "||";
  }

  /**
   * Formats text as inline.
   * @param msg The message you want to make inline.
   * */
  public static String inline(String msg) {
    return  "`" + msg + "`";
  }

  /**
   * Formats text as a code block.
   * @param msg The message you want to make into a codeblock.
   * */
  public static String codeBlock(String msg) {
    return  "```" + msg + "```";
  }

  /**
   * Formats text as a code block.
   * @param msg The message you want to make into a codeblock.
   * @param language The programming language you want to use a formatting.
   * */
  public static String codeBlock(String msg, String language) {
    return  "```" + language + "\n" + msg + "```";
  }

  /**
   * Formats text as a link.
   * @param text The text you want to display.
   * @param link The link that is embeded in the text.
   * */
  public static String link(String text, String link) {
    return "[" + text + "](" + link + ")";
  }

  public static String quote(String text) {
    return "> " + text;
  }

  public static String quote(String text, String author) {
    return "> " + text + "\n> " + author;
  }

  public static String multilineQuote(String... lines) {
    StringBuilder builder = new StringBuilder();
    for (int i = 0; i < lines.length; i++) {
      // Append the value into the builder.
      builder.append(lines[i]);

      // If the value is not the last element of the list then append a new line as well.
      if (i != lines.length - 1) {
        builder.append("\n");
      }
    }

    return ">>> " + builder.toString();
  }

  /**
   * Formats text by encasing it in brackets.
   * @param msg The message you want to encase.
   * */
  public static String encaseBracket(String msg) {
    return "[" + msg + "]";
  }

  /**
   * Formats text by encasing it in parentheses.
   * @param msg The message you want to encase.
   * */
  public static String encaseParentheses(String msg) {
    return "(" + msg + ")";
  }

}
