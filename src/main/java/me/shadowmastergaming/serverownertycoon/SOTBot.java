package me.shadowmastergaming.serverownertycoon;

import com.jagrosh.jdautilities.command.CommandClientBuilder;
import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import java.io.File;
import java.util.Arrays;
import javax.security.auth.login.LoginException;
import me.shadowmastergaming.serverownertycoon.commands.CountryCommand;
import me.shadowmastergaming.serverownertycoon.commands.GiveawayCommand;
import me.shadowmastergaming.serverownertycoon.commands.PickCommand;
import me.shadowmastergaming.serverownertycoon.commands.SetGiveawayRoleCommand;
import me.shadowmastergaming.serverownertycoon.commands.TestCommand;
import me.shadowmastergaming.serverownertycoon.listeners.BotReadyListener;
import me.shadowmastergaming.serverownertycoon.listeners.GiveawayEnterListener;
import me.shadowmastergaming.serverownertycoon.listeners.GuildLeaveListener;
import me.shadowmastergaming.serverownertycoon.listeners.GuildMemberJoinListener;
import me.shadowmastergaming.serverownertycoon.managers.FileManager;
import me.shadowmastergaming.serverownertycoon.tasks.GiveawayTask;
import me.shadowmastergaming.serverownertycoon.utils.BotConstants;
import me.shadowmastergaming.serverownertycoon.utils.Utils;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.requests.GatewayIntent;
import org.bukkit.configuration.file.FileConfiguration;
import org.fusesource.jansi.AnsiConsole;

public class SOTBot {

  private static SOTBot instance;
  private JDA jda;

  private EventWaiter waiter;

  private GiveawayTask giveawayTask;

  private FileManager fileManager;

  public static void main(String[] args) {
    SOTBot sotBot = new SOTBot();
    instance = sotBot;
    sotBot.setup();
  }

  private void setup() {
    String token = Utils.readJSON(BotConstants.auth, "token");
    if (token == null) {
      System.out.println("Invalid token! Cancelling DiscordAPI requests.");
      System.exit(-1);
      return;
    }

    registerClasses();

    fileManager.checkForAndCreateFiles();

    CommandClientBuilder builder = new CommandClientBuilder();
    builder.setPrefix(BotConstants.BOT_PREFIX);
    builder.setOwnerId(BotConstants.BOT_OWNER_ID);
    builder.setCoOwnerIds(BotConstants.BOT_CO_OWNER_ID);
    builder.setActivity(null);

    builder.addCommand(new CountryCommand());
    builder.addCommand(new PickCommand());
    builder.addCommand(new GiveawayCommand(this));
    builder.addCommand(new SetGiveawayRoleCommand(this));
    builder.addCommand(new TestCommand(this));

    builder.useHelpBuilder(false);

    // Attempt to log in and start
    try {
      jda = JDABuilder.create(token, Arrays.asList(GatewayIntent.GUILD_MEMBERS, GatewayIntent.GUILD_MESSAGES,
          GatewayIntent.GUILD_MESSAGE_REACTIONS, GatewayIntent.GUILD_PRESENCES, GatewayIntent.GUILD_VOICE_STATES,
          GatewayIntent.GUILD_EMOJIS, GatewayIntent.DIRECT_MESSAGES))
          .setAutoReconnect(true)
          .addEventListeners(waiter)
          .addEventListeners(new GuildMemberJoinListener())
          .addEventListeners(new GuildLeaveListener())
          .addEventListeners(new BotReadyListener())
          .addEventListeners(new GiveawayEnterListener(this))
          .addEventListeners(builder.build())
          .build().awaitReady();
    } catch (LoginException | IllegalArgumentException | InterruptedException ex) {
      ex.printStackTrace();
      ex.getMessage();
      System.exit(1);
    }

    instance = this;

    registerTasks();
  }

  private void registerTasks() {
    this.giveawayTask = new GiveawayTask(this);
  }

  private void registerClasses() {
    AnsiConsole.systemInstall();

    this.waiter = new EventWaiter();
    this.fileManager = new FileManager(this);
  }

  public static SOTBot getInstance() {
    return instance;
  }

  public EventWaiter getWaiter() {
    return waiter;
  }

  public GiveawayTask getGiveawayTask() {
    return giveawayTask;
  }

  public JDA getJda() {
    return jda;
  }

  public void setJda(JDA jda) {
    this.jda = jda;
  }

  public FileManager getFileManager() {
    return fileManager;
  }

  public void setFileManager(FileManager fileManager) {
    this.fileManager = fileManager;
  }

  public File getDataFolder() {
    return new File(System.getProperty("user.dir"));
  }

  public FileConfiguration getConfig() {
    return fileManager.getConfig();
  }

  public File getGiveawayFile() {
    return fileManager.getDataYMLFile();
  }

  public FileConfiguration getGiveawayConfig() {
    return fileManager.getDataYMLData();
  }
}
